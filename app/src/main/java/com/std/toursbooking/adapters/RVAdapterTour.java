package com.std.toursbooking.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.std.toursbooking.R;
import com.std.toursbooking.models.Tour;

import java.util.List;

public class RVAdapterTour extends RecyclerView.Adapter<RVAdapterTour.TourViewHolder> {
    private List<Tour> listTours;

    public RVAdapterTour(List<Tour> listTours) {
        this.listTours = listTours;
    }

    public class TourViewHolder extends RecyclerView.ViewHolder {
        TextView tvNameTour;
        TextView tvDescriptionTour;

        public TourViewHolder(View itemView) {
            super(itemView);
            tvNameTour = (TextView) itemView.findViewById(R.id.tvNameTour);
            tvDescriptionTour = (TextView) itemView.findViewById(R.id.tvDescriptionTour);
        }
    }

    @Override
    public RVAdapterTour.TourViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tour_list_item, parent, false);
        return new TourViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RVAdapterTour.TourViewHolder holder, int position) {
        holder.tvNameTour.setText(listTours.get(position).getName());
        holder.tvDescriptionTour.setText(listTours.get(position).getDescription());
    }

    @Override
    public long getItemId(int position) {
        return listTours.get(position).getId();
        //return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return listTours.size();
    }
}
