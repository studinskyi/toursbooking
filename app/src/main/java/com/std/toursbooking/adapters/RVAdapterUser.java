package com.std.toursbooking.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.std.toursbooking.R;
import com.std.toursbooking.models.User;

import java.util.List;

public class RVAdapterUser extends RecyclerView.Adapter<RVAdapterUser.UserViewHolder> {
    private List<User> listUsers;

    public RVAdapterUser(List<User> listUsers) {
        this.listUsers = listUsers;
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvLoginUser;
        TextView tvPasswordUser;
        TextView tvIsAdministratorUser;
        TextView tvFirstNameUser;
        TextView tvSurnameUser;
        TextView tvSecondNameUser;
        TextView tvEmailUser;
        TextView tvPhoneUser;

        public UserViewHolder(View itemView) {
            super(itemView);
            tvLoginUser = (TextView) itemView.findViewById(R.id.tvLoginUser);
            tvPasswordUser = (TextView) itemView.findViewById(R.id.tvPasswordUser);
            tvIsAdministratorUser = (TextView) itemView.findViewById(R.id.tvIsAdministratorUser);
            tvFirstNameUser = (TextView) itemView.findViewById(R.id.tvFirstNameUser);
            tvSurnameUser = (TextView) itemView.findViewById(R.id.tvSurnameUser);
            tvSecondNameUser = (TextView) itemView.findViewById(R.id.tvSecondNameUser);
            tvEmailUser = (TextView) itemView.findViewById(R.id.tvEmailUser);
            tvPhoneUser = (TextView) itemView.findViewById(R.id.tvPhoneUser);
        }
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);
        return new UserViewHolder(v);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.tvLoginUser.setText(listUsers.get(position).getLogin());
        holder.tvPasswordUser.setText(listUsers.get(position).getPassword());
        holder.tvIsAdministratorUser.setText((listUsers.get(position).isAdministrator()) ? "администратор" : "пользователь");
        holder.tvFirstNameUser.setText(listUsers.get(position).getFirstName());
        holder.tvSurnameUser.setText(listUsers.get(position).getSurname());
        holder.tvSecondNameUser.setText(listUsers.get(position).getSecondName());
        holder.tvEmailUser.setText(listUsers.get(position).getEmail());
        holder.tvPhoneUser.setText(listUsers.get(position).getPhone());
    }

    @Override
    public long getItemId(int position) {
        return listUsers.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return listUsers.size();
    }
}
