package com.std.toursbooking.screen.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.std.toursbooking.R;
import com.std.toursbooking.adapters.RVAdapterTour;
import com.std.toursbooking.managers.ManagerTours;
import com.std.toursbooking.models.Tour;
import com.std.toursbooking.screen.add.AddTourActivity;

import java.util.ArrayList;
import java.util.List;

public class ToursActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private RecyclerView rvToursList;
    private List<Tour> listTours = new ArrayList<>();
    RVAdapterTour adapter_RV;
    Menu menu_tours_list;

    private String searchSubstr = "";
    //private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tours_activity);

        ///mDatabase = FirebaseDatabase.getInstance().getReference().child("Tours");
        rvToursList = (RecyclerView) findViewById(R.id.rvToursList);
        rvToursList.setHasFixedSize(true);
        LinearLayoutManager linLM = new LinearLayoutManager(this);
        //        linLM.setStackFromEnd(true); // переход на последнюю запись в списке
        //        linLM.setReverseLayout(true); // инверсия списка
        rvToursList.setLayoutManager(linLM);

        filterListTours();

        adapter_RV = new RVAdapterTour(listTours);
        rvToursList.setAdapter(adapter_RV);
    }

    private void filterListTours() {
        listTours.clear();
        listTours.addAll(ManagerTours.getInstance(getBaseContext()).getTours());
        //listTours.addAll(ManagerTours.getInstance(this).getTours());
    }

    @Override
    protected void onResume() {
        super.onResume();

        filterListTours();
        adapter_RV.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        this.menu_tours_list = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tours_list, menu);

        MenuItem searchItem = menu.findItem(R.id.miSearchTour);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // управление видимостью групп и пунктов меню
        //menu.setGroupVisible(R.id.group1, chbVisGroupMenu.isChecked());
        //MenuItem item_logout = menu.findItem(R.id.miLogoutApp);
        //item_logout.setVisible(userCurrent != null);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miAddTour:
                startActivity(new Intent(getBaseContext(), AddTourActivity.class));
                //                // запускает Activity в новом таске. Если уже существует таск с экземпляром данной Activity, то этот таск становится активным, и срабатываем метод onNewIntent().
                //                Intent intentLogin = new Intent(getBaseContext(), LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //                startActivity(intentLogin);
                //                //startActivity(new Intent(getBaseContext(), AddStudentActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchSubstr = query;
        //refreshListInFragment();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchSubstr = newText;
        //refreshListInFragment();
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();

        //        FirebaseRecyclerAdapter<Tour, TourViewHolder> firebaseRecyclerAdapter =
        //                new FirebaseRecyclerAdapter<Tour, TourViewHolder>(Tour.class, R.layout.tour_list_item, TourViewHolder.class, mDatabase) {
        //                    @Override
        //                    protected void populateViewHolder(TourViewHolder viewHolder, Tour model, int position) {
        //                        viewHolder.tvNameTour.setText(model.getName());
        //                        viewHolder.tvDescriptionTour.setText(model.getDescription());
        //                        //                        //            viewHolder.post_desc.setMaxEms(3);
        //                        //                        viewHolder.post_desc.setOnClickListener(
        //                        //                                new View.OnClickListener() {
        //                        //                                    @Override
        //                        //                                    public void onClick(View v) {
        //                        //                                        if (((TextView) v).getMaxLines() == 1)
        //                        //                                            ((TextView) v).setMaxLines(100);
        //                        //                                        else ((TextView) v).setMaxLines(1);
        //                        //                                    }
        //                        //                                }
        //                        //                        );
        //
        //                        //                        Log.d("log", model.image);
        //                        //                        Picasso.with(getBaseContext()).load(model.image)
        //                        //                                .fit()
        //                        //                                .centerInside().into(viewHolder.image);
        //
        //                        //
        //                        //    if ( !model.image.isEmpty())
        //                        //            viewHolder.imageView.setImageURI(Uri.parse(
        //                        //                    model.image));
        //
        //                    }
        //                };
        //        Toast.makeText(getApplicationContext(), "aaaaaa", Toast.LENGTH_SHORT).show();
        //        rvToursList.setAdapter(firebaseRecyclerAdapter);


    }
}
