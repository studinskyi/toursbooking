package com.std.toursbooking.screen.auth;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.std.toursbooking.managers.ManagerUsers;
import com.std.toursbooking.models.User;
import com.std.toursbooking.utils.Utils;

import ru.arturvasilov.rxloader.LifecycleHandler;

public class LoginPresenter {

    private final LifecycleHandler mLifecycleHandler;
    private final ILoginView mILoginView;
    private final Context context;


    public LoginPresenter(@NonNull LifecycleHandler lifecycleHandler,
                          @NonNull ILoginView ILoginView) {
        mLifecycleHandler = lifecycleHandler;
        mILoginView = ILoginView;
        context = mILoginView.getContext();
    }

    // аутентификация пользователя по его настройкам автоматически (при входе в приложение)
    public void init() {

        //        // проверка наличия зарегистрированного пользователя в параметрах преференсес

        //        Utils.showToast(mILoginView.getContext(), "Проверка логина и пароля, открытие доступа к скрытым страницам");
        //        mILoginView.openToursScreen();

        //        String token = PreferenceUtils.getToken();
        //        if (!TextUtils.isEmpty(token)) {
        //            mILoginView.openRepositoriesScreen();
        //        }

        //        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        //        if (sharedPreferences.getBoolean("save_login", false)) {
        //            etLogin.setText(sharedPreferences.getString("user_login",""));
        //        }
        //        if (sharedPreferences.getBoolean("save_password", false)) {
        //            etLogin.setText(sharedPreferences.getString("user_password",""));
        //        }

    }

    public void tryLogIn(@NonNull String login, @NonNull String password) {
        if (TextUtils.isEmpty(login)) {
            mILoginView.showLoginError();
        } else if (TextUtils.isEmpty(password)) {
            mILoginView.showPasswordError();
        } else {
            // проверка логина и пароля, открытие доступа к скрытым страницам
            Log.d(Utils.logTAG, "проверка возможности логина пользователя login: " + login + " password: " + password);
            //            Utils.showToast(context, "Проверка логина и пароля, открытие доступа к скрытым страницам");
            //            mILoginView.openToursScreen();
            login(login, password);

                //            Log.d(Utils.logTAG, "проверка возможности логина пользователя login: " + loginStr + " password: " + passwStr);
            //            if (MainActivity.getUserByLogin(loginStr) == null) {
            //                Log.d(Utils.logTAG, "неудачная попытка входа под логином: " + loginStr + " (пользователя с таким логином не существует)");
            //                Utils.showToast(context, "Пользователь с логином " + loginStr + " не зарегистрирован!");
            //            } else {
            //                if (MainActivity.getUserByLogin(loginStr).getPassword().equals(passwStr)) {
            //                    // установка текущей информации об активном пользователе
            //                    MainActivity.setUserCurrent(MainActivity.getUserByLogin(loginStr));
            //                    //MainActivity.setLoginCurrent(loginStr);
            //                    //MainActivity.setPasswordCurrent(passwStr);
            //                    Log.d(Utils.logTAG, "удачный вход login: " + loginStr + " password: " + passwStr);
            //                    Utils.showToast(context, "удачный вход login: " + loginStr + " password: " + passwStr);
            //
            //                    //                    if (MainActivity.isUserAdministrator()) {
            //                    //                        // далее для пользователя-администратора открывается главная страница,
            //                    //                        Intent intentStartAdmin = new Intent(context, MainActivity.class);
            //                    //                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
            //                    //                        if (sharedPreferences.getBoolean("save_login", false)) {
            //                    //                            sharedPreferences.edit().putString("user_login", MainActivity.getUserCurrent().getLogin());
            //                    //                        }
            //                    //                        if (sharedPreferences.getBoolean("save_password", false)) {
            //                    //                            sharedPreferences.edit().putString("user_password", MainActivity.getUserCurrent().getPassword());
            //                    //                        }
            //                    //                        startActivity(intentStartAdmin);
            //                    //                    } else {
            //                    //                        // а для обычного пользователя открывается профиль
            //                    //
            //                    //                    }
            //
            //                } else {
            //                    // сброс всей информаци об активном пользователе (по причине неверного ввода пароля и логина)
            //                    ManagerUsers.getInstance(context).setUserCurrent(null);
            //                    Log.d(Utils.logTAG, "Введен неверный пароль!");
            //                    Utils.showToast(context, "Введен неверный пароль!");
            //                }
            //            }
            //            //startActivity(new Intent(context, MainActivity.class));

            //            RepositoryProvider.provideGithubRepository()
            //                    .auth(login, password)
            //                    .doOnSubscribe(mILoginView::showLoading)
            //                    .doOnTerminate(mILoginView::hideLoading)
            //                    .compose(mLifecycleHandler.reload(R.id.auth_request))
            //                    .subscribe(authorization -> mILoginView.openRepositoriesScreen(),
            //                            throwable -> mILoginView.showLoginError());
        }
    }

    public User login(@NonNull String loginStr, @NonNull String passwStr) {
        User userFindByLogin = ManagerUsers.getInstance(context).getUserByLogin(loginStr);
        if (userFindByLogin != null) {
            if (userFindByLogin.getPassword().equals(passwStr)) {
                // установка текущей информации об активном пользователе
                ManagerUsers.getInstance(context).setUserCurrent(userFindByLogin);
                Log.d(Utils.logTAG, "удачный вход login: " + loginStr + " password: " + passwStr);
                Utils.showToast(context, "удачный вход login: " + loginStr + " password: " + passwStr);
            } else {
                // сброс всей информаци об активном пользователе (по причине неверного ввода пароля и логина)
                userFindByLogin = null; //
                ManagerUsers.getInstance(context).setUserCurrent(null);
                Log.d(Utils.logTAG, "Введен неверный пароль!");
                Utils.showToast(context, "Введен неверный пароль!");
                //break;
            }
        } else {
            Log.d(Utils.logTAG, "неудачная попытка входа под логином: " + loginStr + " (пользователя с таким логином не существует)");
            Utils.showToast(context, "Пользователь с логином " + loginStr + " не зарегистрирован!");
        }

        return userFindByLogin;
    }
}
