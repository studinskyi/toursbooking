package com.std.toursbooking.screen.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.std.toursbooking.R;
import com.std.toursbooking.managers.ManagerUsers;
import com.std.toursbooking.utils.Utils;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnConfirmRegistration;
    private EditText etLoginReg;
    private EditText etPasswordReg;
    private EditText etConfirmReg;
    private EditText etFirstNameRegistrationStudent;
    private EditText etSurnameRegistrationStudent;
    private EditText etSecondNameRegistrationStudent;

    private CheckBox chbAdministrator;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity);
        context = this;
        etLoginReg = (EditText) findViewById(R.id.editLoginReg);
        etPasswordReg = (EditText) findViewById(R.id.editPasswordReg);
        etConfirmReg = (EditText) findViewById(R.id.editConfirmReg);
        etFirstNameRegistrationStudent = (EditText) findViewById(R.id.etFirstNameRegistrationStudent);
        etSurnameRegistrationStudent = (EditText) findViewById(R.id.etSurnameRegistrationStudent);
        etSecondNameRegistrationStudent = (EditText) findViewById(R.id.etSecondNameRegistrationStudent);

        chbAdministrator = (CheckBox) findViewById(R.id.chbAdministrator);
        chbAdministrator.setChecked(false);

        btnConfirmRegistration = (Button) findViewById(R.id.btnConfirmRegistration);
        btnConfirmRegistration.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        // по id определяем кнопку, вызвавшую этот обработчик
        switch (v.getId()) {
            case R.id.btnConfirmRegistration:
                Log.d(Utils.logTAG, "Нажата кнопка Registration confirm");
                //Toast.makeText(this, "Нажата кнопка Registration confirm", Toast.LENGTH_SHORT).show();

                //tvOut.setText("Нажата кнопка ОК");
                String loginStr = etLoginReg.getText().toString();
                String passwStr = etPasswordReg.getText().toString();
                String confirmPasswStr = etConfirmReg.getText().toString();
                if (loginStr.length() < 6) {
                    Utils.showToast(this, "Логин не может быть менее 6 символов!");
                    break;
                }
                if (ManagerUsers.getInstance(context).getUserByLogin(loginStr) != null) {
                    Utils.showToast(this, "Пользователь с таким логином уже существует!");
                    break;
                }
                if (passwStr.length() < 10) {
                    Utils.showToast(this, "Пароль не может быть менее 10 символов!");
                    break;
                }
                if (!passwStr.equals(confirmPasswStr)) {
                    Utils.showToast(this, "Пароль и его подтверждение не совпадают!");
                    break;
                }
                Log.d(Utils.logTAG, "запуск регистрации по кнопке Registration confirm, "
                        + "для пользователя login: " + loginStr + " password: " + confirmPasswStr);
                //                ManagerUsers.getInstance(context).registrationNewUser(loginStr, confirmPasswStr, chbAdministrator.isChecked()
                //                    , "", "", "", "", "");

                startActivity(new Intent(context, LoginActivity.class));
                break;
        }
    }

    //    private boolean fieldsValidated() {
    //        // пример функции эскейп анализа
    //        int errors = 0;
    //
    //        Validator validator = new Validator();
    //
    //        if (validator.usernameIsCorrect(usernameTIET.getText().toString()) != null)
    //            errors++;
    //
    //        if (validator.passwordIsCorrect(passwordTIET.getText().toString()) != null)
    //            errors++;
    //
    //        boolean errorsPresent = (errors == 0);
    //        int i = 0;
    //        i++;
    //        Log.d("I", "" + i);
    //        if (errorsPresent) showMessage("Incorrect username or password");
    //
    //        return errorsPresent;
    //    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this, "onResume() в активити login_activity", Toast.LENGTH_SHORT).show();
        //setVisibilityOfElements();
    }

    public void setVisibilityOfElements() {
        if (chbAdministrator.isChecked()) {
            etFirstNameRegistrationStudent.setEnabled(false);
            etSurnameRegistrationStudent.setEnabled(false);
            etSecondNameRegistrationStudent.setEnabled(false);
            //etLoginReg.setVisibility(View.INVISIBLE);
        } else {
            etFirstNameRegistrationStudent.setEnabled(true);
            etSurnameRegistrationStudent.setEnabled(true);
            etSecondNameRegistrationStudent.setEnabled(true);
            //etLoginReg.setVisibility(View.INVISIBLE);
        }
    }

    public void onClick_chbAdministrator(View view) {
        setVisibilityOfElements();
    }
}