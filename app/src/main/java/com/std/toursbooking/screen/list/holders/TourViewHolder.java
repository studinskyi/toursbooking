package com.std.toursbooking.screen.list.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.std.toursbooking.R;

public class TourViewHolder extends RecyclerView.ViewHolder {
    View mView;
    TextView tvNameTour;
    TextView tvDescriptionTour;

    public TourViewHolder(View itemView) {
        super(itemView);
        tvNameTour = (TextView) itemView.findViewById(R.id.tvNameTour);
        tvDescriptionTour = (TextView) itemView.findViewById(R.id.tvDescriptionTour);
    }
}
