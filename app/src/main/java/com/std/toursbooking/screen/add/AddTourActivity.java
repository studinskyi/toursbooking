package com.std.toursbooking.screen.add;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.std.toursbooking.R;
import com.std.toursbooking.managers.ManagerTours;
import com.std.toursbooking.utils.Utils;

public class AddTourActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAddTour;
    private EditText etNameTour;
    private EditText etDescriptionTour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_tour_activity);

        etNameTour = (EditText) findViewById(R.id.etNameTour);
        etDescriptionTour = (EditText) findViewById(R.id.etDescriptionTour);
        btnAddTour = (Button) findViewById(R.id.btnAddTour);
        btnAddTour.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddTour:
                String nameStr = etNameTour.getText().toString();
                String descriptionStr = etDescriptionTour.getText().toString();
                if (nameStr.length() < 3) {
                    Utils.showToast(this, "Наименование тура меньше 3 символов!");
                    break;
                }
                if (descriptionStr.length() < 10) {
                    Utils.showToast(this, "Описание тура не может быть менее 10 символов!");
                    break;
                }

                // добавление тура в базу данных
                ManagerTours.getInstance(getBaseContext()).addTour(nameStr,descriptionStr);
                //ManagerDB.addTour(new Tour(nameStr,descriptionStr),getBaseContext());

                finish(); // возврат на страницу списка туров
                //startActivity(new Intent(getBaseContext(), LoginActivity.class));
                break;
        }

    }
}
