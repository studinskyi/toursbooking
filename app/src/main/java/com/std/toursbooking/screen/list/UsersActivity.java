package com.std.toursbooking.screen.list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.std.toursbooking.R;
import com.std.toursbooking.adapters.RVAdapterUser;
import com.std.toursbooking.managers.ManagerUsers;
import com.std.toursbooking.models.User;

import java.util.ArrayList;
import java.util.List;

public class UsersActivity extends AppCompatActivity {

    private RecyclerView rvUsersList;
    private List<User> listUsers = new ArrayList<>();
    RVAdapterUser adapter_RV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_activity);

        ///mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        rvUsersList = (RecyclerView) findViewById(R.id.rvUsersList);
        rvUsersList.setHasFixedSize(true);
        LinearLayoutManager linLM = new LinearLayoutManager(this);
        //        linLM.setStackFromEnd(true); // переход на последнюю запись в списке
        //        linLM.setReverseLayout(true); // инверсия списка
        rvUsersList.setLayoutManager(linLM);

        filterListUsers();

        adapter_RV = new RVAdapterUser(listUsers);
        rvUsersList.setAdapter(adapter_RV);
    }

    private void filterListUsers() {
        listUsers.clear();
        listUsers.addAll(ManagerUsers.getInstance(getBaseContext()).getUsers());
        //listTours.addAll(ManagerTours.getInstance(this).getTours());
    }

    @Override
    protected void onResume() {
        super.onResume();

        filterListUsers();
        adapter_RV.notifyDataSetChanged();
    }


}
