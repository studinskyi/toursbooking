package com.std.toursbooking.screen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.std.toursbooking.R;
import com.std.toursbooking.managers.ManagerTours;
import com.std.toursbooking.managers.ManagerUsers;
import com.std.toursbooking.screen.auth.LoginActivity;
import com.std.toursbooking.screen.list.CustomerReviewsActivity;
import com.std.toursbooking.screen.list.ToursActivity;
import com.std.toursbooking.screen.list.UsersActivity;
import com.std.toursbooking.utils.Utils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private DatabaseReference mDatabase;

    Button btnOpenToursListAct;
    Button btnOpenCustomerReviews;
    Button btnUserList;
    Button btnSaveTour;
    Menu menu_main_activity;

    private Context context = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnOpenToursListAct = (Button) findViewById(R.id.btnOpenToursListAct);
        btnOpenCustomerReviews = (Button) findViewById(R.id.btnOpenCustomerReviews);
        btnUserList = (Button) findViewById(R.id.btnUserList);
        btnSaveTour = (Button) findViewById(R.id.btnSaveTour);

        btnOpenToursListAct.setOnClickListener(this);
        btnOpenCustomerReviews.setOnClickListener(this);
        btnUserList.setOnClickListener(this);

        btnSaveTour.setOnClickListener(this); // тестовая запись тура в базу Firebase
        context = this;

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Tours");
        //        mDatabase = FirebaseDatabase.getInstance().getReference("Tours");
        //        //        FirebaseDatabase database = FirebaseDatabase.getInstance();
        //        //        DatabaseReference myRef = database.getReference("message");

        //        ManagerTours.getInstance(this).addTour("Paris", "романтический тур в Париж");
        //        ManagerTours.getInstance(this).addTour("Rome", "прогулки по улицам Рима");

        initToursInDB();
        initUsersInDB();
    }

    private void initToursInDB() {
        // ДЛЯ ОТЛАДКИ - заполнения базы данных туров для тестирования
        //ManagerTours.getInstance(context).deleteAllTours();
        if (ManagerTours.getInstance(context).getToursCount() < 2) {
            //if (ManagerTours.getInstance(context).getTours().size() < 2) {
            ManagerTours.getInstance(context).addTour("Mexico", "древние руины индейцев Майя");
            ManagerTours.getInstance(context).addTour("Paris", "романтический тур в Париж");
            ManagerTours.getInstance(context).addTour("Rome", "прогулки по улицам Рима");
            ManagerTours.getInstance(context).addTour("Istambul", "красоты проливов Босфор и Дарданеллы");
            ManagerTours.getInstance(context).addTour("Russia - Baykal", "озеро Байкал");
        }
    }

    private void initUsersInDB() {
        // ДЛЯ ОТЛАДКИ - заполнения базы данных пользователей для тестирования
        //ManagerUsers.getInstance(context).deleteAllUsers();
        if (ManagerUsers.getInstance(context).getUsersCount() < 4) {
            //if (ManagerUsers.getInstance(context).getUsers().size() < 4) {
            ManagerUsers.getInstance(context).addUser("admin11", "password11", true, "firstName11", "surname11", "secondName11", "email11@gmail.com", "phone11");
            ManagerUsers.getInstance(context).addUser("admin22", "password22", true, "firstName22", "surname22", "secondName22", "email22@gmail.com", "phone22");
            ManagerUsers.getInstance(context).addUser("user33", "password33", false, "firstName33", "surname33", "secondName33", "email33@gmail.com", "phone33");
            ManagerUsers.getInstance(context).addUser("user44", "password44", false, "firstName44", "surname44", "secondName44", "email44@gmail.com", "phone44");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        this.menu_main_activity = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // управление видимостью групп и пунктов меню
        //menu.setGroupVisible(R.id.group1, chbVisGroupMenu.isChecked());
        MenuItem item_logout = menu.findItem(R.id.miLogoutApp);
        //item_logout.setVisible(userCurrent != null);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miLoginApp:
                //Log.d(getBaseContext().getString(R.id.) getResources().getString(R.id.), "Нажата кнопка Login в activity_main");
                // запускает Activity в новом таске. Если уже существует таск с экземпляром данной Activity, то этот таск становится активным, и срабатываем метод onNewIntent().
                Intent intentLogin = new Intent(getBaseContext(), LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentLogin);
                //startActivity(new Intent(getBaseContext(), AddStudentActivity.class));
                return true;
            case R.id.miLogoutApp:
                //Log.d(TAG, "Нажата кнопка Logout в activity_main");
                // сброс всей информаци об активном пользователе (по причине неверного ввода пароля и логина)
                ManagerUsers.getInstance(context).setUserCurrent(null);
                onResume();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnOpenToursListAct:
                Intent intentToursList = new Intent(context, ToursActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentToursList);
                //startActivity(new Intent(getBaseContext(), ToursActivity.class));
                break;
            case R.id.btnOpenCustomerReviews:
                Intent intentReviewsList = new Intent(context, CustomerReviewsActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentReviewsList);
                break;
            case R.id.btnUserList:
                Intent intentUsersList = new Intent(context, UsersActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentUsersList);
                break;
            case R.id.btnSaveTour:
                String strPeriod = Utils.getCurrentDateTimeString_forFirebase();
                //                Tour tour1 = new Tour("name_" + strPeriod, "tour_city_" + strPeriod);
                //                mDatabase.child("Tours/tour_" + strPeriod).setValue(tour1);
                //        //                mDatabase.child("Tours/1").setValue(tour1);
                //        //                mDatabase.child("Tours/1/name").setValue("Paris");


                //                DatabaseReference newPost = mDatabase.push();
                //                newPost.child("title").setValue("title_val1");
                //                newPost.child("desc").setValue("desc_val1");
                //newPost.child("image").setValue(downloadUrl.toString());

                //                mDatabase.setValue("Hello, World!");
                //                mDatabase.push();

                //                FirebaseDatabase database = FirebaseDatabase.getInstance();
                //                DatabaseReference myRef = database.getReference("Tours");
                //mDatabase.child("Tours/tour_" + strPeriod).setValue("Hello, World! " + strPeriod);

                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("Tours");
                //myRef.child("tour_" + strPeriod + (Math.random() * 100)).setValue("Hello_ " + strPeriod);
                //myRef.setValue("tour_" + strPeriod + (Math.random() * 100));
                //myRef.child("tour_" + strPeriod + (int)(Math.random() * 100)).setValue("Hello_ " + strPeriod);
                myRef.child("tour_" + strPeriod).setValue("Hello_ " + strPeriod);

                //startActivity(new Intent(getBaseContext(), ToursActivity.class));
                break;
        }
    }


}
