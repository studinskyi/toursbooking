package com.std.toursbooking.screen.auth;

import android.content.Context;

import com.std.toursbooking.screen.general.LoadingView;

public interface ILoginView extends LoadingView {

    void openToursScreen();

    Context getContext();

    void showLoginError();

    void showPasswordError();

}
