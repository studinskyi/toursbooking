package com.std.toursbooking.screen.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.std.toursbooking.R;
import com.std.toursbooking.screen.general.LoadingDialog;
import com.std.toursbooking.screen.general.LoadingView;
import com.std.toursbooking.screen.list.ToursActivity;
import com.std.toursbooking.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.arturvasilov.rxloader.LifecycleHandler;
import ru.arturvasilov.rxloader.LoaderLifecycleHandler;

public class LoginActivity extends AppCompatActivity implements ILoginView {

    @BindView(R.id.btnOk)
    Button btnOk;
    @BindView(R.id.btnRegistration)
    Button btnRegistration;
    @BindView(R.id.editLogin)
    EditText etLogin;
    @BindView(R.id.editPassword)
    EditText etPassword;

    @BindView(R.id.tiLoginInputLayout)
    TextInputLayout tiLoginInputLayout;
    @BindView(R.id.tiPasswordInputLayout)
    TextInputLayout tiPasswordInputLayout;

    private LoadingView mLoadingView;
    private LoginPresenter loginPresenter;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        context = this;

        //setSupportActionBar(mToolbar);
        mLoadingView = LoadingDialog.view(getSupportFragmentManager());
        LifecycleHandler lifecycleHandler = LoaderLifecycleHandler.create(this, getSupportLoaderManager());
        loginPresenter = new LoginPresenter(lifecycleHandler, this);
        loginPresenter.init();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //@SuppressWarnings("unused")
    @OnClick({R.id.btnOk, R.id.btnRegistration})
    public void onClick(View v) {
        // по id определяем кнопку, вызвавшую этот обработчик
        switch (v.getId()) {
            case R.id.btnOk:
                Log.d(Utils.logTAG, "Нажата кнопка ОК в активити login_activity");
                //Utils.showToast(context, "Нажата кнопка ОК в активити login_activity");
                String loginStr = etLogin.getText().toString();
                String passwStr = etPassword.getText().toString();
                loginPresenter.tryLogIn(loginStr, passwStr);

                //                Log.d(Utils.logTAG, "проверка возможности логина пользователя login: " + loginStr + " password: " + passwStr);
                //                if (MainActivity.getUserByLogin(loginStr) == null) {
                //                    Log.d(Utils.logTAG, "неудачная попытка входа под логином: " + loginStr + " (пользователя с таким логином не существует)");
                //                    Utils.showToast(context, "Пользователь с логином " + loginStr + " не зарегистрирован!");
                //                    break;
                //                } else {
                //                    if (MainActivity.getUserByLogin(loginStr).getPassword().equals(passwStr)) {
                //                        // установка текущей информации об активном пользователе
                //                        MainActivity.setUserCurrent(MainActivity.getUserByLogin(loginStr));
                //                        //MainActivity.setLoginCurrent(loginStr);
                //                        //MainActivity.setPasswordCurrent(passwStr);
                //                        Log.d(Utils.logTAG, "удачный вход login: " + loginStr + " password: " + passwStr);
                //                        Utils.showToast(context, "удачный вход login: " + loginStr + " password: " + passwStr);
                //                        if (MainActivity.isUserAdministrator()) {
                //                            // далее для пользователя-администратора открывается главная страница,
                //                            Intent intentStartAdmin = new Intent(context, MainActivity.class);
                //                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                //                            if (sharedPreferences.getBoolean("save_login", false)) {
                //                                sharedPreferences.edit().putString("user_login", MainActivity.getUserCurrent().getLogin());
                //                            }
                //                            if (sharedPreferences.getBoolean("save_password", false)) {
                //                                sharedPreferences.edit().putString("user_password", MainActivity.getUserCurrent().getPassword());
                //                            }
                //                            startActivity(intentStartAdmin);
                //                        } else {
                //                            // а для обычного пользователя открывается профиль
                //
                //
                //                        }
                //
                //                    } else {
                //                        // сброс всей информаци об активном пользователе (по причине неверного ввода пароля и логина)
                //                        MainActivity.setUserCurrent(null);
                //                        //MainActivity.setLoginCurrent("");
                //                        //MainActivity.setPasswordCurrent("");
                //                        Log.d(Utils.logTAG, "Введен неверный пароль!");
                //                        Utils.showToast(context, "Введен неверный пароль!");
                //                        break;
                //                    }
                //                }
                //                //startActivity(new Intent(context, MainActivity.class));
                break;
            case R.id.btnRegistration:
                Log.d(Utils.logTAG, "Нажата кнопка Registration в активити login_activity");
                //Utils.showToast(context, "Нажата кнопка Registration в активити login_activity");
                startActivity(new Intent(context, RegistrationActivity.class));
                break;
        }

    }

    //    public static User login(String loginStr, String passwStr, Activity activity) {
    //        User userFindByLogin = MainActivity.getUserByLogin(loginStr);
    //        if (userFindByLogin != null) {
    //            if (userFindByLogin.getPassword().equals(passwStr)) {
    //                // установка текущей информации об активном пользователе
    //                MainActivity.setUserCurrent(userFindByLogin);
    //                //MainActivity.setLoginCurrent(loginStr);
    //                ///MainActivity.setPasswordCurrent(passwStr);
    //                Log.d(Utils.logTAG, "удачный вход login: " + loginStr + " password: " + passwStr);
    //                //Toast.makeText(activity.getBaseContext(), "удачный вход login: " + loginStr + " password: " + passwStr, Toast.LENGTH_SHORT).show();
    //                Utils.showToast(activity.getBaseContext(), "Введен неверный пароль!");
    //            } else {
    //                // сброс всей информаци об активном пользователе (по причине неверного ввода пароля и логина)
    //                userFindByLogin = null; //
    //                MainActivity.setUserCurrent(null);
    //                //MainActivity.setLoginCurrent("");
    //                ///MainActivity.setPasswordCurrent("");
    //                Log.d(Utils.logTAG, "Введен неверный пароль!");
    //                Utils.showToast(activity.getBaseContext(), "Введен неверный пароль!");
    //                //break;
    //            }
    //        } else {
    //            Log.d(Utils.logTAG, "неудачная попытка входа под логином: " + loginStr + " (пользователя с таким логином не существует)");
    //            Utils.showToast(activity.getBaseContext(), "Пользователь с логином " + loginStr + " не зарегистрирован!");
    //        }
    //
    //        return userFindByLogin;
    //    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void showLoading() {
        mLoadingView.showLoading();
    }

    @Override
    public void hideLoading() {
        mLoadingView.hideLoading();
    }

    @Override
    public void showLoginError() {
        tiLoginInputLayout.setError(getString(R.string.errorLogin));
    }

    @Override
    public void showPasswordError() {
        tiPasswordInputLayout.setError(getString(R.string.errorPassword));
    }

    @Override
    public void openToursScreen() {
        Intent intentToursList = new Intent(context, ToursActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intentToursList);
        //finish();
    }
}