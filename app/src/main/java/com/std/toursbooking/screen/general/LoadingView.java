package com.std.toursbooking.screen.general;

public interface LoadingView {

    void showLoading();

    void hideLoading();

}