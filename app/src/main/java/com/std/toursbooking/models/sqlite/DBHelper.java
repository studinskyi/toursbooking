package com.std.toursbooking.models.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "toursdb";
    private static int DATABASE_VERSION = 3; // версия базы данных (необоходимо наращивать при модификации таблиц)
    private static final String NAME_TABLE_tour = "tour"; // название таблицы туров в базе данных
    private static final String NAME_TABLE_user = "user"; // название таблицы пользователей в базе данных
    private static final String KEY_ID = "id";
    // поля таблицы туров
    private static final String KEY_TOUR_NAME = "name";
    private static final String KEY_TOUR_DESCRIPTION = "description";

    public DBHelper(Context context) {
        //new DBHelper(context, "toursdb", null, 3);
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //    public DBHelper(Context context, String name,
    //                    SQLiteDatabase.CursorFactory factory, int version) {
    //        super(context, name, factory, version);
    //    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //sqLiteDatabase.execSQL("CREATE TABLE students(id LONG PRIMARY KEY AUTOINCREMENT, firstname TEXT, surname TEXT, secondname TEXT);");
        //sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS tour (id LONG PRIMARY KEY AUTOINCREMENT, name TEXT, description TEXT);");
        db.execSQL("CREATE TABLE " + NAME_TABLE_tour + " (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, description TEXT);");
        db.execSQL("CREATE TABLE " + NAME_TABLE_user + " (id INTEGER PRIMARY KEY AUTOINCREMENT, login TEXT, password TEXT , isAdministrator NUMERIC, firstName TEXT, surname TEXT, secondName TEXT, email TEXT, phone TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + NAME_TABLE_tour);
        db.execSQL("DROP TABLE IF EXISTS " + NAME_TABLE_user);
        onCreate(db);
    }

    public static int getDatabaseVersion() {
        return DATABASE_VERSION;
    }

    public static void setDatabaseVersion(int databaseVersion) {
        DATABASE_VERSION = databaseVersion;
    }

    public static String getNAME_TABLE_tour() {
        return NAME_TABLE_tour;
    }

    public static String getNAME_TABLE_user() {
        return NAME_TABLE_user;
    }
}
