package com.std.toursbooking.models.sqlite;

import com.std.toursbooking.models.Tour;
import com.std.toursbooking.models.User;

import java.util.List;

public interface IDBAdapter {

    IDBAdapter open();

    void close();

    List<Tour> getTours();

    List<User> getUsers();

    void addTour(Tour tour);

    void addUser(User user);

    void deleteAllUsers();

    void deleteAllTours();

    int getUsersCount();

    int getToursCount();

    User getUserByLogin(String login);

    //    public Contact getContact(int id);
    //    public int updateContact(Contact contact);
    //    public void deleteContact(Contact contact);
}
