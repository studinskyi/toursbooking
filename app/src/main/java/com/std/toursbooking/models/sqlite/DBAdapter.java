package com.std.toursbooking.models.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.std.toursbooking.models.Tour;
import com.std.toursbooking.models.User;

import java.util.ArrayList;
import java.util.List;

public class DBAdapter implements IDBAdapter {

    private DBHelper dbHelper;
    private SQLiteDatabase db;

    public DBAdapter(Context context) {
        //new DBHelper(context, "toursdb", null, 3);
        dbHelper = new DBHelper(context.getApplicationContext());
    }

    public DBAdapter open() {
        //db = dbHelper.getWritableDatabase();
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    private Cursor getAllEntries(String nameTableDB) {
        //        String[] columns = new String[]{DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_NAME, DatabaseHelper.COLUMN_YEAR};
        //        return database.query(DatabaseHelper.TABLE, columns, null, null, null, null, null);
        return db.query(nameTableDB, null, null, null, null, null, null);
    }

    public List<Tour> getTours() {
        ArrayList<Tour> tours = new ArrayList<>();
        Cursor cursor = getAllEntries(DBHelper.getNAME_TABLE_tour());
        if (cursor.moveToFirst()) {
            do {
                tours.add(new Tour(cursor.getLong(cursor.getColumnIndex("id"))
                        , cursor.getString(cursor.getColumnIndex("name"))
                        , cursor.getString(cursor.getColumnIndex("description"))));
            }
            while (cursor.moveToNext());
        }
        cursor.close();

        //        tours.clear();
        //        DBHelper dbHelper = new DBHelper(context);
        //        //DBHelper dbHelper = new DBHelper(context, "toursdb", null, 3);
        //        SQLiteDatabase db;
        //        try {
        //            db = dbHelper.getWritableDatabase();
        //        }
        //        catch (SQLiteException ex){
        //            db = dbHelper.getReadableDatabase();
        //        }
        //        Cursor cursor = db.query("tour", null, null, null, null, null, null);
        //        Tour tour = null;
        //        while (cursor.moveToNext()) {
        //            tour = new Tour(cursor.getLong(cursor.getColumnIndex("id"))
        //                    ,cursor.getString(cursor.getColumnIndex("name"))
        //                    , cursor.getString(cursor.getColumnIndex("description")));
        //            tours.add(tour);
        //        }
        //        dbHelper.close();
        return tours;
    }

    //    public HashMap<String, User> getUsers() {
    //        HashMap<String, User> users = new HashMap<>();
    //        Cursor cursor = getAllEntries(DBHelper.getNAME_TABLE_user());
    //        //db.execSQL("CREATE TABLE " + NAME_TABLE_user + " (id INTEGER PRIMARY KEY AUTOINCREMENT, login TEXT, password TEXT , isAdministrator NUMERIC,
    //        // firstName TEXT, surname TEXT, secondName TEXT, email TEXT, phone TEXT);");
    //        User user = null;
    //        if (cursor.moveToFirst()) {
    //            do {
    //                //                User(String login, String password, Boolean isAdministrator
    //                //                        , String firstName, String surname, String secondName
    //                //                        , String email, String phone)
    //                user = new User(cursor.getString(cursor.getColumnIndex("login")), cursor.getString(cursor.getColumnIndex("password")),
    //                        Boolean.valueOf(cursor.getString(cursor.getColumnIndex("isAdministrator"))),
    //                        cursor.getString(cursor.getColumnIndex("firstName")), cursor.getString(cursor.getColumnIndex("surname")),
    //                                cursor.getString(cursor.getColumnIndex("secondName")), cursor.getString(cursor.getColumnIndex("email")),
    //                                        cursor.getString(cursor.getColumnIndex("phone")));
    //
    //                users.put(user.getLogin(), user);
    //            }
    //            while (cursor.moveToNext());
    //        }
    //        cursor.close();
    //        return users;
    //    }

    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        Cursor cursor = getAllEntries(DBHelper.getNAME_TABLE_user());
        //db.execSQL("CREATE TABLE " + NAME_TABLE_user + " (id INTEGER PRIMARY KEY AUTOINCREMENT, login TEXT, password TEXT , isAdministrator NUMERIC,
        // firstName TEXT, surname TEXT, secondName TEXT, email TEXT, phone TEXT);");
        User user = null;
        if (cursor.moveToFirst()) {
            do {
                user = new User(cursor.getLong(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("login")), cursor.getString(cursor.getColumnIndex("password")),
                        Boolean.valueOf(cursor.getString(cursor.getColumnIndex("isAdministrator"))),
                        cursor.getString(cursor.getColumnIndex("firstName")), cursor.getString(cursor.getColumnIndex("surname")),
                        cursor.getString(cursor.getColumnIndex("secondName")), cursor.getString(cursor.getColumnIndex("email")),
                        cursor.getString(cursor.getColumnIndex("phone")));

                users.add(user);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return users;
    }

    @Override
    public void addTour(Tour tour) {
        ContentValues values = new ContentValues();
        values.put("name", tour.getName());
        values.put("description", tour.getDescription());
        db.insert(DBHelper.getNAME_TABLE_tour(), null, values);
        //                // добавление тура в базу данных
        //                DBHelper dbHelper = new DBHelper(getBaseContext());
        //                //DBHelper dbHelper = new DBHelper(getBaseContext(), "toursdb", null, 1);
        //                SQLiteDatabase db = dbHelper.getWritableDatabase();
        //                db.execSQL("INSERT INTO tour (name,description) VALUES('" + nameStr + "','" + descriptionStr + "');");
        //                dbHelper.close();
    }

    @Override
    public void addUser(User user) {
        //        ManagerDB.addUser(new User(login, password, isAdministrator
        //                , firstName, surname, secondName, email, phone), context);
        ContentValues values = new ContentValues();
        values.put("login", user.getLogin());
        values.put("password", user.getPassword());
        values.put("isAdministrator", user.isAdministrator());
        values.put("firstName", user.getFirstName());
        values.put("surname", user.getSurname());
        values.put("secondName", user.getSecondName());
        values.put("email", user.getEmail());
        values.put("phone", user.getPhone());
        db.insert(DBHelper.getNAME_TABLE_user(), null, values);
    }

    @Override
    public void deleteAllUsers() {
        db.delete(DBHelper.getNAME_TABLE_user(), null, null);

    }

    @Override
    public void deleteAllTours() {
        db.delete(DBHelper.getNAME_TABLE_tour(), null, null);
    }

    @Override
    public int getUsersCount() {
        String countQuery = "SELECT  * FROM " + DBHelper.getNAME_TABLE_user();
        Cursor cursor = db.rawQuery(countQuery, null);
        int countUsers = cursor.getCount();
        cursor.close();

        return countUsers;
    }

    @Override
    public int getToursCount() {
        String countQuery = "SELECT  * FROM " + DBHelper.getNAME_TABLE_tour();
        Cursor cursor = db.rawQuery(countQuery, null);
        int countTours = cursor.getCount();
        cursor.close();

        return countTours;
    }

    @Override
    public User getUserByLogin(String login) {
        User user = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?", DBHelper.getNAME_TABLE_user(), "login");
        Cursor cursor = db.rawQuery(query, new String[]{login});
        if (cursor.moveToFirst()) {
            user = new User(cursor.getLong(cursor.getColumnIndex("id")),
                    cursor.getString(cursor.getColumnIndex("login")), cursor.getString(cursor.getColumnIndex("password")),
                    Boolean.valueOf(cursor.getString(cursor.getColumnIndex("isAdministrator"))),
                    cursor.getString(cursor.getColumnIndex("firstName")), cursor.getString(cursor.getColumnIndex("surname")),
                    cursor.getString(cursor.getColumnIndex("secondName")), cursor.getString(cursor.getColumnIndex("email")),
                    cursor.getString(cursor.getColumnIndex("phone")));
        }
        cursor.close();
        return user;
    }

    //    public User getUserById(long id){
    //        User user = null;
    //        String query = String.format("SELECT * FROM %s WHERE %s=?",DatabaseHelper.TABLE, DatabaseHelper.COLUMN_ID);
    //        Cursor cursor = database.rawQuery(query, new String[]{ String.valueOf(id)});
    //        if(cursor.moveToFirst()){
    //            String name = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NAME));
    //            int year = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_YEAR));
    //            user = new User(id, name, year);
    //        }
    //        cursor.close();
    //        return  user;
    //    }
}
