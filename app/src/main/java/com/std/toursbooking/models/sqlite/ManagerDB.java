package com.std.toursbooking.models.sqlite;

import android.content.Context;

import com.std.toursbooking.models.Tour;
import com.std.toursbooking.models.User;

import java.util.List;

public class ManagerDB {

    // добавление тура в базу данных
    public static void addTour(Tour tour, Context context) {
        DBAdapter adapter = new DBAdapter(context);
        adapter.open();
        adapter.addTour(tour);
        adapter.close();
    }

    // добавление пользователя в базу данных
    public static void addUser(User user, Context context) {
        DBAdapter adapter = new DBAdapter(context);
        adapter.open();
        adapter.addUser(user);
        adapter.close();
    }

    // запрос списка туров из базы данных
    public static List<Tour> getTours(Context context) {
        DBAdapter adapter = new DBAdapter(context);
        adapter.open();
        List<Tour> tours = adapter.getTours();
        adapter.close();
        return tours;
    }

    // запрос списка пользователей из базы данных
    public static List<User> getUsers(Context context) {
        DBAdapter adapter = new DBAdapter(context);
        adapter.open();
        List<User> users = adapter.getUsers();
        adapter.close();
        return users;
    }

    // запрос на удаление всех пользователей из базы данных
    public static void deleteAllUsers(Context context) {
        DBAdapter adapter = new DBAdapter(context);
        adapter.open();
        adapter.deleteAllUsers();
        adapter.close();
    }

    // запрос на удаление всех туров из базы данных
    public static void deleteAllTours(Context context) {
        DBAdapter adapter = new DBAdapter(context);
        adapter.open();
        adapter.deleteAllTours();
        adapter.close();
    }

    public static int getUsersCount(Context context) {
        DBAdapter adapter = new DBAdapter(context);
        adapter.open();
        int countUsers = adapter.getUsersCount();
        adapter.close();

        return countUsers;
    }

    public static int getToursCount(Context context) {
        DBAdapter adapter = new DBAdapter(context);
        adapter.open();
        int countTours = adapter.getToursCount();
        adapter.close();

        return countTours;
    }

    public static User getUserByLogin(String login, Context context) {
        User user = null;
        DBAdapter adapter = new DBAdapter(context);
        adapter.open();
        user = adapter.getUserByLogin(login);
        adapter.close();
        return user;
    }

}
