package com.std.toursbooking.models;

public class Hotel {
    private Long id;
    private String name;


    public Hotel(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return (int) (21 + id * 41);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (!(obj instanceof Hotel))
            return false;

        if (this.id != ((Hotel) obj).getId()) {
            // проверка идентичности группы по полю id
            return false;
        }

        return true;
        //return super.equals(obj);
    }

}
