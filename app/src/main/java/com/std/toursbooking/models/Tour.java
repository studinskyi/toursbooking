package com.std.toursbooking.models;

public class Tour {
    private Long id;
    private String name;
    private String description;

    //    public Tour() {
    //        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
    //    }

    public Tour(String name, String description) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
        this.name = name;
        this.description = description;
    }

    public Tour(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        return (int) (21 + id * 41);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (!(obj instanceof Tour))
            return false;

        if (this.id != ((Tour) obj).getId()) {
            // проверка идентичности группы по полю id
            return false;
        }

        return true;
        //return super.equals(obj);
    }

    public Long getId() {
        return id;
    }
}
