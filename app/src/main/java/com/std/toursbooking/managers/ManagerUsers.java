package com.std.toursbooking.managers;

import android.content.Context;
import android.util.Log;

import com.std.toursbooking.models.User;
import com.std.toursbooking.models.sqlite.ManagerDB;
import com.std.toursbooking.utils.Utils;

import java.util.List;

public class ManagerUsers {

    private User userCurrent; // текущий активный пользователь
    private Context context;
    //private HashMap<String, User> users; // список юзеров

    public ManagerUsers() {
        userCurrent = null;
        //this.users = new HashMap<>();
    }

    // реализация синглтона менеджера через класс-холдер
    private static class ManagerUsersHolder {
        private final static ManagerUsers instance = new ManagerUsers();
    }

    public static ManagerUsers getInstance(Context context) {
        ManagerUsers instance = ManagerUsersHolder.instance;
        if (instance != null)
            if (instance.context == null)
                instance.context = context;

        return instance;
    }

    public boolean isUserAdministrator() {
        if (userCurrent != null)
            return userCurrent.isAdministrator();

        return false;
    }

    public User getUserCurrent() {
        return userCurrent;
    }

    public void setUserCurrent(User userCurrent) {
        this.userCurrent = userCurrent;
    }

    public void addUser(String login, String password, Boolean isAdministrator
            , String firstName, String surname, String secondName
            , String email, String phone) {

        ManagerDB.addUser(new User(login, password, isAdministrator
                , firstName, surname, secondName, email, phone), context);

        Log.d(Utils.logTAG, "зарегистрирован новый пользователь login: " + login + " password: " + password);
    }

    public List<User> getUsers() {
        List<User> users = ManagerDB.getUsers(context);
        //        DBHelper helper = new DBHelper(context, "toursdb", null, 3);
        //        // подключаемся к БД
        //        SQLiteDatabase database = helper.getWritableDatabase();
        //        //SQLiteDatabase database = helper.getWritableDatabase();
        //        Cursor cursor = database.query("tour", null, null, null, null, null, null);
        //        Tour tour = null;
        //        while (cursor.moveToNext()) {
        //            tour = addTour(cursor.getString(cursor.getColumnIndex("name"))
        //                    , cursor.getString(cursor.getColumnIndex("description")));
        //            tours.add(tour);
        //        }
        //        // закрываем подключение к БД
        //        helper.close();
        return users;
    }

    public void deleteAllUsers() {
        ManagerDB.deleteAllUsers(context);
    }

    public int getUsersCount() {
        return ManagerDB.getUsersCount(context);
    }

    public User getUserByLogin(String login) {
        return ManagerDB.getUserByLogin(login,context);
    }

    //    public User registrationNewUser(String login, String password, boolean isAdministrator
    //            , String firstName, String surname, String secondName
    //            , String email, String phone) {
    //        User userNew = new User(login, password, isAdministrator, firstName, surname, secondName, email, phone);
    //        if (userNew != null) {
    //            Log.d(Utils.logTAG, "зарегистрирован новый пользователь login: " + login + " password: " + password);
    //            users.put(login, userNew);
    //        }
    //        return userNew;
    //    }


}
