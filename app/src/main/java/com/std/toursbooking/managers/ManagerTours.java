package com.std.toursbooking.managers;

import android.content.Context;
import android.util.Log;

import com.std.toursbooking.models.Tour;
import com.std.toursbooking.models.sqlite.ManagerDB;
import com.std.toursbooking.utils.Utils;

import java.util.List;

public class ManagerTours {
    //private List<Tour> tours;
    private Context context;

    public ManagerTours() {
        //this.tours = new ArrayList<>();
    }

    // реализация синглтона менеджера через класс-холдер
    private static class ManagerToursHolder {
        private final static ManagerTours instance = new ManagerTours();
    }

    public static ManagerTours getInstance(Context context) {
        ManagerTours instance = ManagerToursHolder.instance;
        if (instance != null)
            if (instance.context == null)
                instance.context = context;

        return instance;
    }

    public void addTour(String name, String description) {
        ManagerDB.addTour(new Tour(name,description),context);
        Log.d(Utils.logTAG, "зарегистрирован новый тур name: " + name + " description: " + description);
    }

    public List<Tour> getTours() {
        return ManagerDB.getTours(context);
    }

    public void deleteAllTours() {
        ManagerDB.deleteAllTours(context);
    }

    public int getToursCount() {
        return ManagerDB.getToursCount(context);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }
}
